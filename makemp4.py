"""Make MP4. Creates MP4 video from directory (or directory tree) containing time-lapse photos.

Usage:
  makemp4.py SOURCEDIR MP4NAME
  makemp4.py SOURCEDIR MP4NAME [--time-offset=SECONDS] [--morph-factor=NUMFRAMES] [--csv-datafile=CSVFILENAME]

Options:
  -h --help                   Show this screen.
  --version                   Show version.
  --time-offset=SECONDS       Number of seconds between camera time and system time
  --morph-factor=NUMFRAMES    Number of intermediate frames to create for smoothness
  --csv-datafile=CSVFILENAME  Name of file containing CSV data for this flight
"""
import os, re, shutil, sys, subprocess
from datetime import datetime,timedelta
from pytz import timezone
import PIL
from PIL import ExifTags, Image, ImageDraw, ImageFont
from docopt import docopt
import tarfile


def purge(dir, pattern):
    for f in os.listdir(dir):
    	if re.search(pattern, f):
    		os.remove(os.path.join(dir, f))

def archive(tarname,fnames):
    # Archives the files given by name
    if tarname.endswith(".tar"):
        dirname=tarname[:-4]
    else:
        raise IOError, "tarfile name should end with '.tar'"
    
    with tarfile.open(tarname,mode="w") as tar:
        for fn in fnames:
            print(fn)
            bname=os.path.split(fn)[-1]
            tar.add(fn,arcname=os.path.join(dirname,bname))

def get_file_list(sourcedir):
    # return full list of files sorted 
    # by file name, not path

    fnames=[]
    fpaths=[]

    for root, dirs, files in os.walk(sourcedir):
        for ff in files:
            if ff.lower().endswith('.jpg'):
                fnames.append(ff)
                fpaths.append(os.path.join(root,ff))

    sorti=sorted(range(len(fnames)), key=fnames.__getitem__)

    return [fpaths[i] for i in sorti]

def annotate_and_shrink(thisf,newf,time_offset=0,csvdata={},fontsize=64):

    img=Image.open(thisf)
    exif={PIL.ExifTags.TAGS[k]: v
            for k, v in img._getexif().items()
            if k in PIL.ExifTags.TAGS}

    dt=datetime.strptime(exif['DateTimeOriginal'],"%Y:%m:%d %H:%M:%S")-\
            timedelta(0,time_offset)

    tz=timezone('Etc/GMT+10')
    tz.localize(dt)
    tstamp=dt.strftime("%Y-%m-%d %H:%M:%S %Z")

    try:
        data=csvdata[dt.strftime("%Y-%m-%d %H:%M:%S")]
    except KeyError:
        data={}

    notes=[tstamp]
    notes.extend(["%10s: %7.2f"%(kk,vv) for kk,vv in zip(data.keys(),\
            data.values())])

    nlines=len(notes)
    draw  = ImageDraw.Draw(img)
    font  = ImageFont.truetype("FreeMonoBold.ttf", fontsize, encoding="unic")
    
    recheight=fontsize*nlines
    draw.rectangle((0,exif['ExifImageHeight'],750,exif['ExifImageHeight']-\
            recheight),fill='#ffffff')

    for lineno,note in enumerate(notes):
        draw.text( (10,exif['ExifImageHeight']-recheight+lineno*fontsize),\
                note, font=font, fill='#ff00ff')

    img.thumbnail((800,600),Image.ANTIALIAS)
    img.save(newf,"JPEG")

def annotate_and_shrink_wrapper(fnames,destdir,time_offset=0,csvname=None):
    # Annotates each image and saves
    # an 800x600 version in destdir

    # Read text data
    if csvname is not None:
        csvdata=read_csv_data(csvname)
    else:
        csvdata={}

    counter=0
    output=[]

    for thisf in sorted(fnames):
        bname=os.path.split(thisf)[-1]
        try: assert bname.lower().endswith('.jpg')
        except: continue

        # only iterate once file is verfied as a jpg
        counter+=1
        newf=os.path.join(destdir,("%d"%counter).zfill(6)+".jpg")
        annotate_and_shrink(thisf,newf,time_offset,csvdata)
        output.append(newf)
        
    return output

def read_csv_data(csvname):

    varlookup=dict(zip(('StaticPress_Fuselage',\
            'PressureAltitudeCorrected_Fuselage','AmbientTemp_TP3S',\
            'DewPointTemp_TP3S'),('Pres (hPa)','Alt (m)','Temp (C)','Dwpt (C)')))

    fid=open(csvname)
    lines=fid.readlines()
    headerkeys=lines[1].rstrip(',\n').split(',')[1:]
    dummy=dict(zip([varlookup[kk] for kk in headerkeys],[-999.]*len(headerkeys)))
    output={}

    for line in lines[2:]:
        ll=line.rstrip(',\n').split(',')
        ts=ll[0]
        output[ts]=dict(zip([varlookup[kk] for kk in headerkeys],[float(vv) for vv in ll[1:]]))
    return output



if __name__=='__main__':
    arguments = docopt(__doc__, version='Make MP4 0.1')

    # Non-optional arguments
    sourcedir=arguments['SOURCEDIR']
    mp4name=arguments['MP4NAME']

    # Put the time offset in seconds here (i.e. the time that 
    # the GOPRO is ahead of the system time)
    if arguments['--time-offset'] is None:
        time_offset=0
    else:
        time_offset=int(arguments['--time-offset'])

    # Morph factor. This specifies the number 
    # of intermediate frames created. If zero,
    # then the morph step is skipped and the files
    # are just renamed. 
    if arguments['--morph-factor'] is None:
        morph_factor=0
    else:
        morph_factor=int(arguments['--morph-factor'])

    csv_datafile=arguments['--csv-datafile']

    # get destdir from output file name
    destdir=mp4name.strip('.mp4')

    try: assert os.path.isdir(destdir)
    except AssertionError: os.makedirs(destdir)

    try: assert not os.path.isfile(mp4name)
    except AssertionError: os.remove(mp4name)

    # get originals, shrink and annotate
    fnames=get_file_list(sourcedir)
    nnames=annotate_and_shrink_wrapper(fnames,destdir,time_offset,csv_datafile)
    
    ## Debug only! get the names of the 'new' files
    ## having previously run annotate_and_shrink()
    # nnames=[os.path.join(destdir,nn) for nn in os.listdir(destdir)]

    # "morphing" step: creates a set of 
    # images that transition more smoothly.

    if morph_factor == 0:
        for nn in nnames:
            no=os.path.split(nn)[-1]
            fn=os.path.join(destdir,no.strip(".jpg")+".morph.jpg")
            shutil.copy(nn,fn)
    elif morph_factor >= 1:
        morphcmd="convert %s/*.jpg -morph %d %s/%%06d.morph.jpg"%(destdir,morph_factor,destdir)
        print(morphcmd)
        status = subprocess.call(morphcmd,shell=True)
    else:
        raise ValueError, "morph factor must be an integer >= 0"

    ffmpegcmd="ffmpeg -r 25 -i %s %s"%(os.path.join(destdir,"%06d.morph.jpg"),mp4name)
    print(ffmpegcmd)
    status = subprocess.call(ffmpegcmd,shell=True)

    purge(destdir,"morph")
